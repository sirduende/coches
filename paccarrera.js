$(document).ready(function () {   

    // 1 - CONSTANTES 

    //Constantes del juego
    const maxCoches = 9;
    const meta = 800;

    //Variables del juego
    var lugarFinal = 1;

    //Nombres de los id de los elementos de pantalla que voy a manejar, si cambio el nombre no tengo que cambiarlo en cada sitio
    const idSelectCoches = "#coches";
    const idDivCarretera = "#carrera";
    const idDivResultados = "#resultados";
    const idButtonIniciar = "#iniciar";
    const idButtonReiniciar = "#reiniciar";

    // 2 - FUNCIONES

    //Array con imágenes de coches que utilizaré en carrera
    function obtenerImagenesCoches() {
        var imgCoches = new Array();
        for (var i = 0; i < maxCoches; i++) {
            imgCoches[i] = "img/car" + (i + 1) + "-min.png";
        }
        return imgCoches;
    }

    function prepararCochesSalida() {
        var imagenesCoches = obtenerImagenesCoches();
        var numParticipantes = $(idSelectCoches).val();

        $(idDivCarretera).html(''); //Vaciar carretera
        for (var i = 0; i < numParticipantes; i++) {
            var div = "<div class='coche'><a href='" + imagenesCoches[i] + "'>" + "<img id='car" + i + "'src='" + imagenesCoches[i] + "'></a></div>";
            $(idDivCarretera).append(div);
        }
    }

    function correr() {
        var numParticipantes = $(idSelectCoches).val();
        for (var i = 0; i < numParticipantes; i++) {
            moverCoche(i);
        }
    }

    function getPosicionCoche(idCoche) {
        return parseInt($("#car" + idCoche).css("marginLeft").replace("px", ""));//El margen viene en px y necesitamos un valor numérico
    }

    function moverCoche(idCoche) {
        var posicionActual = getPosicionCoche(idCoche);
        if (posicionActual >= meta) {
            return;
        }
        var desplazamiento = Math.floor(Math.random() * 10) + 1;
        var posicionFinal = posicionActual + desplazamiento;

        $("#car" + idCoche).animate({ marginLeft: posicionFinal + "px" }, 2, function () {
            //CallBack
            if (getPosicionCoche(idCoche) >= meta) {
                $("#resultado-"+idCoche).text(lugarFinal+"º");
                lugarFinal++;
                return;
            }
            moverCoche(idCoche);
        });
    }

    // 3 - INICIO Y EVENTOS JQUERY

    //Evento cambio combo
    $(idSelectCoches).change(prepararCochesSalida);       

    //Botón iniciar. Carrera: desplazar los coches hacia la derecha con el método animate()
    $(idButtonIniciar).click(function () {
        prepararCochesSalida(); //Si el usuario no ha seleccionado al inicio, corren los de por defecto
        correr();
        lugarFinal = 1;
        $(idButtonIniciar).hide();
        $(idButtonReiniciar).show();
        $(idDivResultados).show();
    });

    //Botón reiniciar: al pulsarlo resetea la carrera, desaparece y se muestra iniciar
    $("#reiniciar").click(function () {
        $("img").stop(); //Si está en movimiento
        $("img").css({ marginLeft: "0px" });
        $(idButtonReiniciar).hide();
        $(idButtonIniciar).show();
    });

    
    

});